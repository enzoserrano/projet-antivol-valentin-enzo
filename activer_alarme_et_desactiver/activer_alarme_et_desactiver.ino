//const int TouchPin= A0;
const int PinLed=5;
volatile int state = LOW; // déclaration d'une variable volatile

void setup()
{
Serial.begin(9600);
pinMode(PinLed, OUTPUT);
attachInterrupt(0, blink, RISING); // pour désactiver/ activer alarme
attachInterrupt(1, change, LOW); //pour activer l'alarme
}


void loop()
{

digitalWrite(PinLed, state); // la LED reflète l'état de la variable (OFF dans un premier temps)
}


void blink() // la fonction appelée par l'interruption externe n°0 (capteur bouton)
{
state = !state; // inverse l'état de la variable
}

void change() { //fonction pour interruption externe n°1 (capteur de toucher)
  state = !state;
  
}
