#include <Wire.h>
#include <rgb_lcd.h>
#include <LGPS.h>
#include <LGSM.h>    

rgb_lcd lcd;
gpsSentenceInfoStruct info;

char buff[256];
int num;
unsigned long t = 3000;
unsigned long change;
byte scrno;
boolean main = true;

static unsigned char getComma(unsigned char num, const char *str) {
  unsigned char i, j = 0;
  int len = strlen(str);
  for (i = 0; i < len; i ++)
  {
    if (str[i] == ',')
      j++;
    if (j == num)
      return i + 1;
  }
  return 0;
}

static double getDoubleNumber(const char *s) {
  char buf[10];
  unsigned char i;
  double rev;

  i = getComma(1, s);
  i = i - 1;
  strncpy(buf, s, i);
  buf[i] = 0;
  rev = atof(buf);
  return rev;
}

static double getIntNumber(const char *s) {
  char buf[10];
  unsigned char i;
  double rev;

  i = getComma(1, s);
  i = i - 1;
  strncpy(buf, s, i);
  buf[i] = 0;
  rev = atoi(buf);
  return rev;
}

void parseGPGGA(const char* GPGGAstr) {
  double latitude;
  double longitude;
  double dilution, alt, hg;
  int tmp, hour, minute, second;
  if (GPGGAstr[0] == '$') {
    tmp = getComma(1, GPGGAstr);
    hour     = (GPGGAstr[tmp + 0] - '0') * 10 + (GPGGAstr[tmp + 1] - '0');
    minute   = (GPGGAstr[tmp + 2] - '0') * 10 + (GPGGAstr[tmp + 3] - '0');
    second    = (GPGGAstr[tmp + 4] - '0') * 10 + (GPGGAstr[tmp + 5] - '0');

    sprintf(buff, "UTC timer %2d-%2d-%2d", hour, minute, second);
    Serial.println(buff);

    tmp = getComma(2, GPGGAstr);
    latitude = getDoubleNumber(&GPGGAstr[tmp]);
    tmp = getComma(4, GPGGAstr);
    longitude = getDoubleNumber(&GPGGAstr[tmp]);

    int latdeg = latitude / 100;
    int londeg = longitude / 100;
    double latmin = latitude - (((double)latdeg) * 100);
    double lonmin = longitude - (((double)londeg) * 100);

    tmp = getComma(7, GPGGAstr);
    num = getIntNumber(&GPGGAstr[tmp]);
    sprintf(buff, "satellites number = %d ", num);
    Serial.println(buff);

    sprintf(buff, "Lat=%dd %5.4fm", latdeg, latmin);
    Serial.println(buff);

    sprintf(buff, "Lon=%dd %5.4fm", londeg, lonmin);
    Serial.println(buff);

    tmp = getComma(8, GPGGAstr);
    dilution = getDoubleNumber(&GPGGAstr[tmp]);
    sprintf(buff, "Horizontal dilution = %10.4f ", dilution);
    Serial.println(buff);

    tmp = getComma(9, GPGGAstr);
    alt = getDoubleNumber(&GPGGAstr[tmp]);
    sprintf(buff, "Altitude (meters) = %10.4f ", alt);
    Serial.println(buff);

    tmp = getComma(11, GPGGAstr);
    hg = getDoubleNumber(&GPGGAstr[tmp]);
    sprintf(buff, "Height from mean sea level (geoid) = %10.4f", hg);
    Serial.println(buff);
    Serial.println("====================================================================");

    if (millis() - change > t) {
      if (main) {
        sprintf(buff, "Lat: %dd %5.4fm", latdeg, latmin);
        sprintf(buff, "Lon: %dd %5.4fm", londeg, lonmin);

        t = 3000;
        scrno++;
        if (scrno > 2) {
          scrno = 1;
        }
        main = false;
      }
      else {
        switch (scrno) {
          case 1:
            sprintf(buff, "Altitude: %5.4f", alt, buff, "UTC: %2d-%2d-%2d", hour, minute, second);
            break;
        }
        t = 1000;
        main = true;
      }
      change = millis();
    }
  }
  else
  {
    
  }
}

String startBuffer;                                              
String messageConfirm = "What do you want to send?";              
String message;                                                   
String confirmSend = "Do you want to send the SMS (Yes or No)?";  
String confirmReply;

void setup() {
Serial.begin(115200); ;
LGPS.powerOn() ;

  
  while (Serial.available() == 0) {}    
  startBuffer = Serial.readString();    

  Serial.println("Starting Send SMS!"); 
  
  while (!LSMS.ready())                 
  {
    delay(1000);                        
  }
  
  Serial.println("Sim initialized");    

  LSMS.beginSMS("0693225260");


}

void loop() {
Serial.println ("LGPS loop") ;

LGPS.getData(&info) ;
parseGPGGA ((const char*) info. GPGGA);

Serial.println((char*) info. GPGGA);       
  while (Serial.available() == 0) {}    
  message = Serial.readString();        

  Serial.println(confirmSend);          
  while (Serial.available() == 0) {}    
  confirmReply = Serial.readString();
  
  LSMS.print((char*) info. GPGGA);   

  if (confirmReply == "Yes") {          
    if (LSMS.endSMS())                  
    {
      Serial.println("SMS sent");    
    }
    else
    {
      Serial.println("SMS is not sent");
    }
  }
  else {
    Serial.println("SMS sending cancelled by user");
    if (LSMS.endSMS())                  
    {
      Serial.println("SMS sent");    
    }
  }
  Serial.println(""); //Prints an empty line just for good formatting, to start again
  
}
